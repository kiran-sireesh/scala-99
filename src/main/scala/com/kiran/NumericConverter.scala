package com.kiran

import scala.annotation.tailrec
import scala.collection.immutable.TreeMap

object NumericConverter {

  val romanSymbolMap = TreeMap(
    1 -> "I",
    4 -> "IV",
    5 -> "V",
    9 -> "IX",
    10 -> "X",
    50 -> "L",
    100 -> "C",
    1000 -> "M")(implicitly[Ordering[Int]].reverse)


  def toRoman(num: Int): String = {
    @tailrec
    def romanRec(n: Int, acc: String = ""): String = {
      val romanAccAndRemainingNumPair = romanSymbolMap
        .find(_._1 <= n)
        .map(x => (x._2 * (n / x._1), n % x._1))
      romanAccAndRemainingNumPair match {
        case Some((roman, decimal)) => romanRec(decimal, acc + roman)
        case None => acc
      }
    }
    romanRec(num)
  }

  implicit class RicherInt(i: Int) {
    def toRoman = NumericConverter.toRoman(i)
  }

}
