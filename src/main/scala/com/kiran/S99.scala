package com.kiran

import scala.annotation.tailrec


object S99 {

  @tailrec
  def last(list: List[Int]): Int = list match {
    case Nil => throw new NoSuchElementException
    case last :: Nil => last
    case x :: xs => last(xs)
  }

  @tailrec
  def penultimate(l: List[Int]): Int = l match {
    case Nil => throw new NoSuchElementException
    case last :: Nil => throw new NoSuchElementException
    case p :: _ :: Nil => p
    case x :: xs => penultimate(xs)
  }

}
