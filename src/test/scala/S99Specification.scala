import org.scalacheck.Properties
import org.scalacheck.Prop.{forAll, BooleanOperators}
import com.kiran.S99._

object S99Specification extends Properties("") {

  property("last element of list") = forAll { l: List[Int] =>
    l.nonEmpty ==> (last(l) == l.last)
  }

  property("penultimate element of list") = forAll { l: List[Int] =>
    (l.size > 1) ==> (penultimate(l) == l(l.size - 2))
  }

}
