import com.kiran.NumericConverter.RicherInt
import org.scalatest.FlatSpec

class RomanNumeralsConversionSpec extends FlatSpec {
  "Roman number converter" should "work for single symbols" in {
    assert(1.toRoman == "I")
    assert(5.toRoman == "V")
    assert(10.toRoman == "X")
    assert(50.toRoman == "L")
    assert(100.toRoman == "C")
    assert(1000.toRoman == "M")
  }
  "Roman number converter" should "work for multiple of single symbols" in {
    assert(3.toRoman == "III")
    assert(4.toRoman == "IV")
    assert(9.toRoman == "IX")
    assert(20.toRoman == "XX")
    assert(50.toRoman == "L")
    assert(100.toRoman == "C")
    assert(1000.toRoman == "M")
  }
}
